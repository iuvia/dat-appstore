#!/usr/bin/env python3

import os
import subprocess
import sys

import click

# If these directories do not exist, we look them up from the AppImage path by creating {name}.data
APPDIR = os.environ.setdefault("APPDIR", os.path.dirname(os.path.abspath(sys.argv[0])))

APPIMAGE_ARGV0 = os.environ.setdefault(
    "APPIMAGE_ARGV0", os.path.abspath(sys.argv[0])
)

APPIMAGE_CODENAME = os.environ.setdefault("APPIMAGE_CODENAME", "io.iuvia.market.stub")

APPIMAGE_LOCATION = os.environ.setdefault(
    "APPIMAGE", os.path.dirname(os.path.abspath(sys.argv[0]))
)
APPIMAGE_DATA_LOCATION = APPIMAGE_LOCATION + ".data" + os.path.sep

STATE_DIRECTORY = os.environ.setdefault(
    "STATE_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "lib")
)
RUNTIME_DIRECTORY = os.environ.setdefault(
    "RUNTIME_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "run")
)
CACHE_DIRECTORY = os.environ.setdefault(
    "CACHE_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "cache")
)
LOGS_DIRECTORY = os.environ.setdefault(
    "LOGS_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "log")
)
CONFIGURATION_DIRECTORY = os.environ.setdefault(
    "CONFIGURATION_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "etc")
)

os.environ.setdefault('MARKETPLACE_HTTP_PORT', os.path.join(APPIMAGE_DATA_LOCATION + "run/http.sock"))


@click.group()
@click.option("--iuvia-platform", is_flag=True)
def cli(iuvia_platform):
    if not iuvia_platform:
        raise Exception("Unknown usage")


@cli.command("metadata")
@click.option("--icon", "-i", nargs=1)
def metadata(icon: str = None):
    if not icon:
        icon = "metadata.bin"
    elif not icon.endswith(".png"):
        raise IOError("Can only request png images")

    full_path = os.path.join(APPDIR, icon)
    size = os.path.getsize(full_path)
    with open(full_path, "rb") as f:
        return os.sendfile(1, f.fileno(), offset=0, count=size)


@cli.command("install")
def platform_install():
    click.echo("Nothing to do")


@cli.command("env")
def platform_env():
    click.echo("AppDir is: " + APPDIR)


@cli.command("uninstall")
def platform_uninstall():
    click.echo(
        "Nothing to do. Remaining data is in {APPDATA}".format(APPDATA=STATE_DIRECTORY,)
    )


@cli.command("run")
def platform_run():
    os.chdir(os.path.join(APPDIR, "nodejs-server-server"))
    os.makedirs(STATE_DIRECTORY, exist_ok=True)
    print("GOT ENV: ", os.environ)
    subprocess.check_output(
        [
            "node",
            "index.js",
        ],
        env=os.environ,
    )

if __name__ == "__main__":
    cli()
