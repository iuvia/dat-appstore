'use strict';

const { DatArchive } = require('dat-sdk/auto')

async function get_bundled_data(archive) {
    if(!archive) {
	archive = await DatArchive.load('dat://mkt-dev.iss.iuvia.io')
    }
    return JSON.parse(await archive.readFile('/bundled.json', {encoding: 'utf8'}))
}


/**
 * Retrieve the requested IUVIA Platform AppImage from upstream.
 * Returns the AppImage binary
 *
 * codename String codename of the IPAI to return
 * version String version of the IPAI to return
 * returns IPAI
 **/
exports.getIPAVersionData = async function(codename, version) {
    try {
	const archive = await DatArchive.load('dat://mkt-dev.iss.iuvia.io')
	const bundled_data = await get_bundled_data(archive)
	const items = bundled_data.filter(f => f.content.codename == codename && f.content.version == version)
	if (items.length == 0) {
	    throw ({'error': {'status': 404}, 'codename': codename, 'version': version})
	}
	const f = items[0]
	return await archive.readFile('/' + f.path, 'binary')
    } catch(e) {
	console.log(e)
	if(e.error && e.error.status) {
	    throw e
	}
	throw ({'error': {'status': 500}, 'msg': e})
    }
}


/**
 * Retrieve a specific IUVIA Platform AppImage (IPAI) informatio.
 * Returns a single IPAI
 *
 * codename String codename of the IPAI to return
 * version String version of the IPAI to return
 * returns IPAI
 **/
exports.getIPAVersionInfo = async function(codename,version) {
    try {
	const archive = await DatArchive.load('dat://mkt-dev.iss.iuvia.io')
	const bundled_data = await get_bundled_data(archive)
	const items = bundled_data.filter(f => f.content.codename == codename && f.content.version == version)
	if (items.length == 0) {
	    throw ({'error': {'status': 404}})
	}
	const f = items[0]
	const c = f.content
	let download_status = 'unknown'
	const stat = await archive.stat('/' + f.path)
	if (stat.blocks == stat.downloaded) {
	    download_status = 'local'
	} else {
	    download_status = 'remote got ' + stat.downloaded + ' blocks out of ' + stat.blocks
	}
	return {
	    'release_date': '2020-02-20T00:00:00',
	    'codename': c.codename,
	    'version': c.version,
	    'download_status': download_status,
	    'developer_key': c.developer,
	    'url': '/ipa/' + c.codename + '/versions/' + c.version,
	    'signatures': {
		'sha512sum': 'PASS'
	    }
	}
    } catch(e) {
	console.log(e)
	if(e.error && e.error.status) {
	    throw e
	}
	throw ({'error': {'status': 500}, 'msg': e})
    }
}

/**
 * Retrieve available versions of IPAs. Each version is a IUVIA Platform AppImage (IPAI).
 * Returns available IPA versions.
 *
 * codename String codename of the IPA to return
 * returns IPAIList
 **/
exports.getIPAVersions = function(codename) {
  return new Promise(async function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "pages" : 0,
  "_total_items" : 6,
  "_links" : {
    "next_page" : "next_page",
    "previous_page" : "previous_page"
  },
  "items" : [ {
    "release_date" : "2000-01-23T04:56:07.000+00:00",
    "codename" : "codename",
    "download_status" : "download_status",
    "developer_key" : "developer_key",
    "version" : "version",
    "url" : "url",
    "signatures" : {
      "sha512sum" : "sha512sum",
      "sha256sum" : "sha256sum"
    }
  }, {
    "release_date" : "2000-01-23T04:56:07.000+00:00",
    "codename" : "codename",
    "download_status" : "download_status",
    "developer_key" : "developer_key",
    "version" : "version",
    "url" : "url",
    "signatures" : {
      "sha512sum" : "sha512sum",
      "sha256sum" : "sha256sum"
    }
  } ]
};
	try {
	    const archive = await DatArchive.load('dat://mkt-dev.iss.iuvia.io')
	    const bundled_data = await get_bundled_data(archive)
	    // Check that there exist at least one version with such codename
	    if(bundled_data.filter(f => f.content.codename == codename).length == 0) {
		reject({'error': {'status': 404}})
	    }
	    resolve({
		'pages': 1,
		'_total_items': bundled_data.length,
		'items': await Promise.all(bundled_data
					   .filter(f => f.content.codename == codename)
					   .map(async f => {
					       const c = f.content
					       let download_status = 'unknown'
					       try {
						   const stat = await archive.stat('/' + f.path)
						   if (stat.blocks == stat.downloaded) {
						       download_status = 'local'
						   } else {
						       download_status = 'remote got ' + stat.downloaded + ' blocks out of ' + stat.blocks
						   }
					       } catch(e) {
						   console.log("DID NOT GET FILE: ", f.path)
					       }
					       return {
						   'release_date': '2020-02-20T00:00:00',
						   'codename': c.codename,
						   'version': c.version,
						   'download_status': download_status,
						   'developer_key': c.developer,
						   'url': '/ipa/' + c.codename + '/versions/' + c.version,
						   'signatures': {
						       'sha512sum': 'PASS'
						   }
					       }
					   }))
	    })
	} catch(e) {
	    console.log(e)
	    reject({'error': {'status': 500}, 'msg': e})
	}
  })
}


/**
 * Retrieve available versions of IPAs. Each version is a IUVIA Platform AppImage (IPAI).
 * Returns a single pet
 *
 * codename String codename of the IPAI to return
 * version String version of the IPAI to return
 * action String Action to perform in the IPAI
 * returns IPAI
 **/
exports.performIPAIAction = async function(codename,version,action) {
    if (action == 'retrieve') {
	try {
	    const archive = await DatArchive.load('dat://mkt-dev.iss.iuvia.io')
	    const bundled_data = await get_bundled_data(archive)
	    bundled_data
		.filter(f => f.content.codename == codename && f.content.version == version)
		.map(async f => {
		    const c = f.content
		    let download_status = 'unknown'
		    const stat = await archive.stat('/' + f.path)
		    if (stat.blocks == stat.downloaded) {
			download_status = 'local'
		    } else {
			await archive.download('/' + f.path)
		    }
		})
	    return await exports.getIPAVersionInfo(codename, version)
	} catch(e) {
	    console.log(e)
	    throw ({'error': {'status': 500}, 'msg': e})
	}
    } else {
	throw ({'error': {'status': 400}, 'msg': 'Unkown action'})
    }

    return new Promise(function(resolve, reject) {
	var examples = {};
	examples['application/json'] = {
	    "release_date" : "2000-01-23T04:56:07.000+00:00",
	    "codename" : "codenamex",
	    "download_status" : "download_status",
	    "developer_key" : "developer_key",
	    "version" : "version",
	    "url" : "url",
	    "signatures" : {
		"sha512sum" : "sha512sum",
		"sha256sum" : "sha256sum"
	    }
	};
	if (Object.keys(examples).length > 0) {
	    resolve(examples[Object.keys(examples)[0]]);
	} else {
	    resolve();
	}
    });
}

